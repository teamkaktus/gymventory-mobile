controllers.controller('asideCtrl', function($scope, $state, $ionicSideMenuDelegate, clubFactory, $ionicPlatform, AuthFactory, userProfile, $window, $rootScope, ConfigApi, Socket, ngDialog, clubFactory, CreditsFactory) {
    userProfile.getUserData().then(

        function(response) {
            var userData = response.data;
            $scope.sideBar = {
                avatar: (userData.avatar_type == 1) ? ConfigApi.serverStatic + userData.avatar || 'img/no_avatar_vopros.jpg' : userData.avatar,
                userStatus: userData.status,
                firstname: userData.first_name,
                lastname: userData.last_name,
                address: userData.address
            };
        },
        function(error) {
            console.log('error');
        }
    );
    $scope.closeThis = function() {
        $scope.favoriteList = false;
    }
 /*   $scope.hides = function () {
      $scope.leftSidebar = '';
    }*/
    $scope.toggleF = function() {
        if (!$scope.favoriteList) {
            clubFactory.getLastClubs().then(function(response) {
                if (response.data.status) {
                    $scope.lastVisited = response.data.rows;
                    $scope.favoriteList = true;
                } else {
                    $scope.favoriteList = false;
                }
            }, function(err) {
                $scope.favoriteList = false;

            })
        } else {
            $scope.favoriteList = false;
        }
    }
    $rootScope.$on('$stateChangeStart',
        function(event, toState, toParams, fromState, fromParams) {
            Socket.getUnreaded()
              $scope.leftSidebar = '';
        })
    $scope.toggleClass = function($event, className) {
        $ionicSideMenuDelegate.toggleLeft();
    };

    $scope.toggleRight = function($event, className) {
        className =  'toggled customAsside';

        if ($scope.rightSidebar == undefined || $scope.rightSidebar == '') {
            $scope.rightSidebar = className;
        } else {
            $scope.rightSidebar = '';
        }
    };

    $(document).on('click', '.pane.customAsside', function(){
        $scope.rightSidebar = '';
        $scope.$apply();
    })



    $scope.rightSidebarInvite = 'toggled';

    $scope.toggleInvite = function($event, className) {
        className = className || 'toggled';
        if ($scope.rightSidebarInvite == undefined || $scope.rightSidebarInvite == '') {
            $scope.rightSidebarInvite = className;
        } else {
            $scope.rightSidebarInvite = '';
        }
    };

    $rootScope.$on('notification', function(event, data) {
        console.log(data);
    });
    // if (AuthFactory.authenticated()&&!Socket.logged) {
    //   Socket.login(jwtHelper.decodeToken(AuthFactory.getToken()).id);
    // }
    $rootScope.$on('not-update', function(event, count) {
        $scope.notificationCount += count;
        $scope.$apply()
    });
    $rootScope.$on('not-unreaded', function(event, count) {
        $scope.notificationCount = count;
        $scope.$apply()
    });
    $rootScope.$watch('unreadedN', function(data) {
        $scope.notificationCount = 0;
        if ($rootScope.unreadedN) {
            $scope.notificationCount += $rootScope.unreadedN;
        }
    });
    $rootScope.$on('update-unreaded', function() {
        $scope.unreadedCount = 0;
        Object.keys($rootScope.unreaded).forEach(function(key) {
            $scope.unreadedCount += $rootScope.unreaded[key]
        });
        $scope.$apply()
    });

    $rootScope.$watch('unreaded', function() {
        $scope.unreadedCount = 0;
        Object.keys($rootScope.unreaded).forEach(function(key) {
            $scope.unreadedCount += $rootScope.unreaded[key]
        });
    });

    $scope.signout = function() {
        AuthFactory.removeToken();
        $state.go('start');
    };
    $scope.goEditProfile = function() {
        $state.go('app.profile');
    };

    $scope.manage = {
        cg_count: false
    };
    clubFactory.checkClub().then(

        function(response) {
            if (parseInt(response.data.count) > 0) {
                $scope.manage.cg_count = true;
                return true;
            } else {
                $scope.manage.cg_count = false;
                return false;
            }
        },
        function(error) {
            console.log(error);
        }
    );
    $scope.cc = {
        credits_val: 0
    };
    CreditsFactory.getBalance().then(

        function(response) {
            if (response.data.balance) {
                $scope.cc.credits_val = response.data.balance;
            }
        }
    );


    $scope.$on('setBalance', function(event, count) {
        $scope.cc.credits_val = count;
        console.log(count);
    });

    $scope.$on('resetBalance', function(event) {
        CreditsFactory.getBalance().then(

            function(response) {
                if (response.data.balance) {
                    $scope.cc.credits_val = response.data.balance;
                }
            }
        );
    });

    $scope.replenish_balance = function() {
        var dialog = ngDialog.open({
            templateUrl: './templates/pp-cc-paymentTpl.html',
            className: 'ngdialog-theme-default',
            controller: 'pp_paymentCtrl',
            scope: $scope
        });
    };

    $scope.$on('replenish_balance', function(event, count) {
        $scope.replenish_balance();
    });

    userProfile.getUserTransactions().then(
        function (response) {
            $scope.transactionsCount = response.data.length;
        }
    )
});
