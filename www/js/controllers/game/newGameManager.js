controllers.controller('newGameManagerCtrl', function ($scope, $rootScope, $stateParams, $timeout, $filter, GamesFactory, gymFactory, $timeout, $state) {

    var id = $stateParams.id;
    console.log(id);
    $scope.game = {
        dt: new Date(),
        start_time: 11,
        end_time: 14,
        age: {
            min: 15,
            max: 40
        },
        gender: 'All',
        type: '',
        gym_id: ''
    }

    gymFactory.getGym(id).then(
        function (response) {
            console.log('getGym ', response)
            $scope.game.type = response.data[0].type;
            $scope.game.gym_id = response.data[0].id;
            $scope.game.price = response.data[0].price;
            $scope.game.price_for_member = response.data[0].price_for_member;
            $scope.gymsObj = response.data;
        },
        function (error) {
            console.log(error);
        }
    )

    var setSliders = function(date_val){

        $scope.startStepArray = ['01:00 am', '01:15 am', '01:30 am', '01:45 am', '02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 pm', '12:15 pm', '12:30 pm', '12:45 pm', '01:00 pm', '01:15 pm', '01:30 pm', '01:45 pm', '02:00 pm', '02:15 pm', '02:30 pm', '02:45 pm', '03:00 pm', '03:15 pm', '03:30 pm', '03:45 pm', '04:00 pm', '04:15 pm', '04:30 pm', '04:45 pm', '05:00 pm', '05:15 pm', '05:30 pm', '05:45 pm', '06:00 pm', '06:15 pm', '06:30 pm', '06:45 pm', '07:00 pm', '07:15 pm', '07:30 pm', '07:45 pm', '08:00 pm', '08:15 pm', '08:30 pm', '08:45 pm', '09:00 pm', '09:15 pm', '09:30 pm', '09:45 pm', '10:00 pm', '10:15 pm', '10:30 pm', '10:45 pm', '11:00 pm', '11:15 pm', '11:30 pm', '11:45 pm', '12:00 am', '12:15 am', '12:30 am', '12:45 am'];
        $scope.startTime = {
            id: 's_time',
            value: "08:00 am",
            value2: "02:00 pm",
            options: {
                stepsArray: $scope.startStepArray,
                precision: 2,
                minLimit: 0,
                maxLimit: 100,
                hideLimitLabels: true,
                showSelectionBar: true,
                draggableRange: true,
                onEnd: function (sliderId, modelValue) {
                    $scope.game.start_time = $filter('convertTime')($scope.startTime.value);
                    $scope.game.end_time = $filter('convertTime')($scope.startTime.value2);

                }
            }
        };


        $scope.endStepArray = ['01:00 am', '01:15 am', '01:30 am', '01:45 am', '02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 pm', '12:15 pm', '12:30 pm', '12:45 pm', '01:00 pm', '01:15 pm', '01:30 pm', '01:45 pm', '02:00 pm', '02:15 pm', '02:30 pm', '02:45 pm', '03:00 pm', '03:15 pm', '03:30 pm', '03:45 pm', '04:00 pm', '04:15 pm', '04:30 pm', '04:45 pm', '05:00 pm', '05:15 pm', '05:30 pm', '05:45 pm', '06:00 pm', '06:15 pm', '06:30 pm', '06:45 pm', '07:00 pm', '07:15 pm', '07:30 pm', '07:45 pm', '08:00 pm', '08:15 pm', '08:30 pm', '08:45 pm', '09:00 pm', '09:15 pm', '09:30 pm', '09:45 pm', '10:00 pm', '10:15 pm', '10:30 pm', '10:45 pm', '11:00 pm', '11:15 pm', '11:30 pm', '11:45 pm', '12:00 am', '12:15 am', '12:30 am', '12:45 am'];
        $scope.endTime = {
            id: 'e_time',
            value: '11:00 pm',
            options: {
                stepsArray: $scope.endStepArray,
                precision: 2,
                minLimit: 0,
                maxLimit: 100,
                hideLimitLabels: true,
                showSelectionBar: true,
                onEnd: function (sliderId, modelValue) {
                    var time = $filter('convertTime')(modelValue);

                    $scope.game.end_time = time;
                }
            }
        };
    }

    $scope.ageSlider = {
        minValue: $scope.game.age.min,
        maxValue: $scope.game.age.max,
        options: {
            hideLimitLabels: true,
            showSelectionBar: true,
            floor: 10,
            ceil: 70,
            step: 1,
            onEnd: function (sliderId, minValue, maxValue) {
                $scope.game.age.min = minValue;
                $scope.game.age.max = maxValue;
            }
        }
    }

    $scope.today = function () {
        $scope.game.dt = new Date();
        setSliders($scope.game.dt);
    };
    $scope.today();

    $scope.clear = function () {
        $scope.game.dt = null;
    };

    $scope.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };
    function disabled(data) {
        var date = data.date,
            today = new Date();
        now = new Date(today.getFullYear(),today.getMonth(),today.getDate());
        var status = true;
        if(date >= now)
            status = false;
        return status;
    }

    $scope.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yyyy',
        min: new Date(),
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1,
        showWeeks: false
    };

    $scope.toggleMin = function () {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };

    $scope.toggleMin();

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function (year, month, day) {
        $scope.game.dt = new Date(year, month, day);
        setSliders(new Date(year, month, day));
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'fullDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
    ];

    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }

    $scope.createGame = function(data){
        console.log(data);
        GamesFactory.ManagerCreateGame(data).then(
            function(response){
                console.log(response.data.recordId);
                $scope.closeThisDialog();
                $rootScope.$emit('game_added', {id: response.data.recordId});
            },
            function(error){
                console.log(error);
            }
        )
    }
});
