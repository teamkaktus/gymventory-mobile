controllers
    .controller('edit-profileCtrl', function ($scope, $state, ConfigApi, userProfile, AuthFactory, $timeout, clubFactory, $filter) {

        $scope.heightData = {
            ft: "1'",
            in: '5"'
        };
        $scope.userData = {
            email: '',
            first_name: '',
            last_name: '',
            password: '',
            img: 'img/avatar_icon.png',
            tel: '',
            telCode: '+1',
            gender: 'Female',
            height: $scope.heightData.ft + $scope.heightData.in,
            birth: '',
            wld: '',
            education: '',
            hometown: '',
            lkf: '',
            ft: '',
            jn: ''
        };

        $scope.profile = {
            phone: ''
        };
        userProfile.get().then(
            function (response) {
                var profile = response.data;
                $scope.profile = {
                    avatar: profile.avatar,
                    img: (profile.avatar_type == 1) ? ConfigApi.serverStatic + profile.avatar:profile.avatar,
                    firstname: profile.first_name,
                    lastname: profile.last_name,
                    email: profile.email,
                    password: '',
                    dial_code: profile.dial_code,
                    phone: profile.phone || '',
                    address: profile.address,
                    gender: profile.gender || '',
                    dayS: (profile.dob)?profile.dob.split("-")[0]:'',
                    monS: (profile.dob)?profile.dob.split("-")[1]:'',
                    yearS: (profile.dob)?profile.dob.split("-")[2]:'',
                    h: profile.height || '',
                    avatar_type: profile.avatar_type,
                    wld: profile.wld,
                    education: profile.education,
                    hometown: profile.hometown,
                    lkf: profile.lkf,
                    ft: profile.ft,
                    jn: profile.jn
                };
                $scope.userData = {
                    telCode: $scope.profile.dial_code
                }
            },
            function (error) {
                console.log(error);
            }
        );
        $scope.daysInMonth = function (month, year) {
            return new Date(year, month, 0).getDate();
        };


        $scope.dob = {
            day: [],
            dayS: 1,
            month: [],
            monthS: 1,
            year: [],
            yearS: 1990
        };

        $scope.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        $scope.error = {
            email: '',
            first_name: '',
            last_name: '',
            password: '',
            img: '',
            gender: '',
            height: '',
            birth: ''
        };
        AuthFactory.telephone()
            .then(
                function (response) {
                    $scope.telephone = response.data;
                },
                function (error) {
                    console.log('error');
                }
            );
        $scope.dateBuild = function () {
            $scope.dob.day = []
            for (var i = 1; i <= $scope.daysInMonth($scope.dob.monthS, $scope.dob.yearS); i++) {
                $scope.dob.day.push(i);
            }
            if (!$scope.dob.month.length) {
                for (var i = 1; i <= 12; i++) {
                    $scope.dob.month.push(i);
                }
            }
            if (!$scope.dob.year.length) {
                for (var i = 1950; i <= new Date().getFullYear(); i++) {
                    $scope.dob.year.push(i);
                }
            }
            $scope.userData.dob = $scope.dob.monthS + '-' + $scope.dob.dayS + '-' + $scope.dob.yearS
        };
        $scope.dateBuild();
        $scope.checkForm = function (form) {
            if (form.$invalid) {
                angular.forEach(form.$error, function (field) {
                    angular.forEach(field, function (errorField) {
                        errorField.$setTouched();
                    })
                });
                return false;
            } else {
                return true;
            }
        };

        $scope.loading_spiner = false;
        $scope.success_text = false;
        var timer;

        $scope.editProfile = function (form) {
            if($scope.profile.address != null){
                if($scope.profile.address.formatted_address){
                    $scope.profile.address = $scope.profile.address.formatted_address;
                }
            }else{
                $scope.profile.address = '';
            }

            $scope.success_text = false;
            $scope.loading_spiner = true;
            clubFactory.getAddressCoordinate($scope.profile.address).then(
                function (response) {
                    $scope.profile.coordinates = response.data.results[0].geometry.location;
                    if ($scope.checkForm(form)) {
                        userProfile.update($scope.profile).then(
                            function (response) {
                                $scope.success_text = true;
                                $scope.loading_spiner = false;
                                $scope.inform_text = 'You profile saved!';
                                timer = $timeout(function () {
                                    $scope.success_text = false;
                                    //$state.reload();
                                    delete(timer);
                                }, 5000);

                            },
                            function (error) {
                                $scope.success_text = true;
                                $scope.inform_text = 'There was an error when saving!';
                                $scope.loading_spiner = false;
                                timer = $timeout(function () {
                                    $scope.success_text = false;
                                    delete(timer);
                                }, 5000);
                            }
                        );
                    }
                },
                function(error){
                    $scope.success_text = true;
                    $scope.inform_text = 'Not valid address!';
                    $scope.loading_spiner = false;
                    timer = $timeout(function() {
                        $scope.success_text = false;
                        ngDialog.closeAll(true);
                        delete(timer);
                    }, 5000);
                }
            );
        };
        $scope.file_uploaded = function ($file, $message) {
            $scope.profile.avatar_type = 1;
            $scope.profile.avatar = JSON.parse($message)['file'].path;
        };

        // $scope.$watch('profile.phone', function(value, oldValue) {
        //     value = String(value);
        //     var number = value.replace(/[^0-9]+/g, '');
        //     $scope.profile.phone = $filter('phonenumber')(number);
        // });

    });
