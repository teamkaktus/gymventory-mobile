controllers.controller('b_club_requestCtrl', function($scope, $rootScope, clubFactory, $state) {
    $scope.requests = {};
    clubFactory.getClubRequest().then(
        function (response) {
            $scope.requests = response.data.result;
        }
    )

    $scope.accept_request = function ($event, club, user) {
        clubFactory.setAcceptRequest({club_id: club, user_id: user}).then(
            function (response) {
                $state.reload();
            }
        )
    }
    $scope.decline_request = function ($event, club, user) {
        clubFactory.setDeclineRequest({club_id: club, user_id: user}).then(
            function (response) {
                $state.reload();
            }
        )
    }
});
