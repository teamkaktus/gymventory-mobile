controllers.controller('club-classes_itemCtrl', function ($scope, $rootScope, GamesFactory, ngDialog, $stateParams, clubFactory, gymFactory, $timeout, $state) {
    var class_id = $stateParams.id;
    $scope.data = {
        showDelete: false
    };

    console.log(class_id);

    $scope.check_club = false;
    $scope.inform_text = false;

    $rootScope.club = {
        create: false
    };

    $scope.gameList = {};

    clubFactory.countClub().then(
        function (response) {
            if (parseInt(response.data.count) >= 1) {
                $rootScope.club.create = false;
                console.log("count club", $rootScope.club.create)
            } else {
                $rootScope.club.create = true;
                if ($rootScope.club.create) {
                    $state.go('app.createMyClub');
                }
            }
        },
        function (error) {
            console.log(error);
        }
    );

    gymFactory.getGym(class_id).then(
        function (response) {
            console.log('getGym ', response)
            $scope.gymsObj = response.data;
        },
        function (error) {
            console.log(error);
        }
    )

    $scope.update_gym = function (data, id) {
        data.id = id;

        var loading_spiner = true;
        success_text = false;

        $scope.loading_spiner = loading_spiner;
        $scope.success_text = success_text;

        gymFactory.updateGym(data).then(
            function (response) {
                $scope.success_text = true;
                $scope.loading_spiner = false;
                $scope.inform_text = 'This class updated!';
                timer = $timeout(function () {
                    $scope.success_text = false;
                    delete(timer);
                }, 5000);
                console.log(response);
            },
            function (error) {
                $scope.success_text[id] = true;
                $scope.inform_text = 'There was an error when saving!';
                $scope.loading_spiner = false;
                timer = $timeout(function () {
                    $scope.success_text = false;
                    delete(timer);
                }, 5000);
                console.log(error);
            }
        )
    };

    GamesFactory.getManagerGames(class_id).then(
        function (response) {
            $scope.gameList = response.data.items;
            console.log('getManagerGames', response);
        }
    )

    $rootScope.$on('game_added', function (event, obj) {
        GamesFactory.getManagerGames(class_id).then(
            function (response) {
                $scope.gameList = response.data.items;
                console.log('getManagerGames', response);
            }
        )
    });

    $scope.createGame = function () {
        ngDialog.open({
            width: 900,
            templateUrl: './templates/game/newGameManager.html',
            className: 'ngdialog-theme-default',
            controller: 'newGameManagerCtrl'
        });
    };

    $scope.deleteGame = function (item, id) {
        GamesFactory.ManagerDeleteGame(id).then(
            function(response){
                console.log('deleteGame', response.data);
                $scope.gameList.splice($scope.gameList.indexOf(item), 1);
            },
            function(error){
                console.log('deleteGame', error);
            }
        )
    }
});
