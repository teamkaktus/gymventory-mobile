controllers
    .controller('club-pageCtrl',  function ($scope, $state, $rootScope, ConfigApi, userProfile, $filter, ngDialog, AuthFactory, toaster, $stateParams, GamesFactory, $timeout, FiltersFactory, myGeocode, clubFactory, $ionicLoading) {
        var club_id = $stateParams.club_id;
        var timer;

        $scope.marker = {};
        $scope.clubReguest = false;

        $scope.breakpoints = [
            {
                breakpoint: 350,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: false,
                    initialSlide: 0
                }
            },
            {
                breakpoint: 450,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 3,
                    infinite: false,
                    initialSlide: 0
                }
            },
            {
                breakpoint: 567,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 3,
                    infinite: false,
                    initialSlide: 0
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 7,
                    slidesToScroll: 3,
                    infinite: false
                }
            }
        ];

        $scope.dates_arr = [];
        $scope.addFavorite = function(club_id,index) {
          clubFactory.addFavorite(club_id).then(function (res) {
            if (res.data.status) {
              if (res.data.rows.affectedRows) {
                    $scope.club_cards.favorite = 1;
                    // $scope.club_favorite_cards.push($scope.club_cards[index]);
                    // $scope.club_cards.splice(index,1);
                $
              }else {
                var dialog = ngDialog.open({
                    templateUrl: './templates/notification/show_textTpl.html',
                    className: 'ngdialog-theme-default',
                    controller: 'show_textCtrl',
                    scope: $scope
                });
              }
            }
            console.log(res);
          },function (err) {
            $scope.text = {
              text:"Error",
              description:"Db Error"
            }
            var dialog = ngDialog.open({
                templateUrl: './templates/notification/show_textTplS.html',
                className: 'ngdialog-theme-default',
                scope: $scope
            });

          })
        }
        $scope.removeFavorite = function(club_id,index) {
          clubFactory.removeFavorite(club_id).then(function (res) {
            if (res.data.status) {
              if (res.data.rows.affectedRows) {
                    $scope.club_cards.favorite = 0
                    // $scope.club_favorite_cards.push($scope.club_cards[index]);
                    // $scope.club_cards.splice(index,1);
                $
              }else {
                $scope.text = {
                  text:"Error",
                  description:"Db Error"
                }
                var dialog = ngDialog.open({
                    templateUrl: './templates/notification/show_textTplS.html',
                    className: 'ngdialog-theme-default',
                    scope: $scope
                });
              }
            }
            console.log(res);
          },function (err) {
            $scope.text = {
              text:"Error",
              description:"Db Error"
            }
            var dialog = ngDialog.open({
                templateUrl: './templates/notification/show_textTplS.html',
                className: 'ngdialog-theme-default',
                scope: $scope
            });

          })
        }
        $scope.set_timers = function (data) {
            var date_tmp = '';
            if(data) {
                data.forEach(function (el, i, arr) {
                    if(date_tmp == ''){
                        if(el.date && el.game_id) {
                            $scope.dates_arr[el.game_id] = el.date;
                            date_tmp = el.date;
                        }else{
                            $scope.dates_arr[0] = $scope.filter.dt;
                            date_tmp = $scope.filter.dt;
                        }
                    }
                    if (el.date) {
                        if(date_tmp != el.date && el.date && el.game_id){
                            $scope.dates_arr[el.game_id] = el.date;
                            date_tmp = el.date;
                        }
                        var startDate = new Date(el.date);
                        var dd = startDate.getDate();
                        var mm = startDate.getMonth() + 1; //January is 0!
                        var yyyy = startDate.getFullYear();
                        var endtime = yyyy + '-' + mm + '-' + dd + ' ' + el.start_time.toString().replace('.', ':') + ':00';


                        function getTimeRemaining(endtime) {

                            var t = Date.parse(endtime) - Date.parse(new Date());
                            var seconds = Math.floor((t / 1000) % 60);
                            var minutes = Math.floor((t / 1000 / 60) % 60);
                            var hours = Math.floor(t / (1000 * 60 * 60));


                            return {
                                'total': t,
                                'hours': hours,
                                'minutes': minutes,
                                'seconds': seconds
                            };
                        }

                        $scope.clock = [];
                        $scope.clock[el.game_id] = "00:00:00";
                        var tick = function () {
                            var t = getTimeRemaining(endtime);
                            $scope.clock[el.game_id] = t.hours + ':' + t.minutes + ':' + t.seconds;
                            timer = $timeout(tick, 1000); // reset the timer
                        }
                        timer = $timeout(tick, 1000);
                    }
                })
            }
        }

        $scope.en_time = $filter('filterClockSlider')((new Date).getHours());
        $scope.tab_club_page = "Gym";

        $scope.filter = {
            club_id: club_id,
            sport: {
                1: false,
                2: false,
                3: false,
                4: false,
                5: false,
                6: false,
                7: false,
                8: false,
                9: false,
                10: false,
                11: false,
                12: false,
                13: false,
                14: false,
                15: false,
                16: false
            },
            access: 'All',
            start_time : 0,
            end_time : 23.45,
            sort: 'Soonest',
            age: {
                min: 20,
                max: 60
            },
            dt: '',
            gender: 'All'
        };
        //console.log("online", $rootScope.users.online);

        $scope.clearFil = function () {
            $scope.filter.sport = {
                1: false,
                2: false,
                3: false,
                4: false,
                5: false,
                6: false,
                7: false,
                8: false,
                9: false
            };
            $scope.filter.access = 'All';
            $scope.filter.start_time = 0;
            $scope.filter.end_time = 23.45;
            $scope.filter.age = {
                min: 20,
                max: 60
            };
            $scope.filter.gender = 'All';
            $scope.filter.sort = 'Soonest';

            setSliders();

            $scope.filter.dt = '';
            $scope.setFilter();
        };

        $scope.myPosition = true;
        $scope.myAddress = true;
        $scope.distanceDisabled = true;
        $scope.distanceClass = 'disabled';

        $scope.club_cards = [];


        if (!navigator.geolocation) {
            $scope.myPosition = false;
            return false;
        }

        var set_time = function (arg, arr, date_val) {
            console.log('date_val', date_val);
            var to_hour = (new Date).getHours();
            var limit = 0,
                start = "08:00 am",
                end = "11:00 pm";
            switch(arg){
                case "start":{
                    if((new Date).getMonth() == (date_val).getMonth() && (new Date).getDate() == (date_val).getDate()){
                        limit = arr.indexOf($filter('filterClockSlider')(to_hour+1));
                        start = $filter('filterClockSlider')(to_hour+1);
                    }else{
                        limit = 0;
                    }

                    return {start: start, limit: limit};
                }
                case "end":{
                    if((new Date).getMonth() == ($scope.filter.dt).getMonth() && (new Date).getDate() == ($scope.filter.dt).getDate()){
                        limit = arr.indexOf($filter('filterClockSlider')(to_hour+2));
                        //end = $filter('filterClockSlider')(23);
                    }else{
                        limit = 0;
                    }
                    return {end: end, limit: limit};
                }
            }
        }
        var setSliders = function(date_val){
            $scope.startStepArray = ['12:00 am', '12:15 am', '12:30 am', '12:45 am', '01:00 am', '01:15 am', '01:30 am', '01:45 am', '02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 pm', '12:15 pm', '12:30 pm', '12:45 pm', '01:00 pm', '01:15 pm', '01:30 pm', '01:45 pm', '02:00 pm', '02:15 pm', '02:30 pm', '02:45 pm', '03:00 pm', '03:15 pm', '03:30 pm', '03:45 pm', '04:00 pm', '04:15 pm', '04:30 pm', '04:45 pm', '05:00 pm', '05:15 pm', '05:30 pm', '05:45 pm', '06:00 pm', '06:15 pm', '06:30 pm', '06:45 pm', '07:00 pm', '07:15 pm', '07:30 pm', '07:45 pm', '08:00 pm', '08:15 pm', '08:30 pm', '08:45 pm', '09:00 pm', '09:15 pm', '09:30 pm', '09:45 pm', '10:00 pm', '10:15 pm', '10:30 pm', '10:45 pm'];
            $scope.startTime = {
                id: 's_time',
                value: "12:00 am",
                options: {
                    stepsArray: $scope.startStepArray,
                    precision: 2,
                    minLimit: 0,
                    maxLimit: 100,
                    hideLimitLabels: true,
                    showSelectionBar: true,
                    draggableRange: true,
                    onEnd: function (sliderId, modelValue) {
                        var time = $filter('convertTime')(modelValue);
                        if (time>=24) {
                            time-=24
                        }

                        $scope.filter.start_time = time;
                        $scope.en_time = modelValue;
                        $scope.setFilter();


                    }
                }
            };
            $scope.endStepArray = ['01:00 am', '01:15 am', '01:30 am', '01:45 am', '02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 pm', '12:15 pm', '12:30 pm', '12:45 pm', '01:00 pm', '01:15 pm', '01:30 pm', '01:45 pm', '02:00 pm', '02:15 pm', '02:30 pm', '02:45 pm', '03:00 pm', '03:15 pm', '03:30 pm', '03:45 pm', '04:00 pm', '04:15 pm', '04:30 pm', '04:45 pm', '05:00 pm', '05:15 pm', '05:30 pm', '05:45 pm', '06:00 pm', '06:15 pm', '06:30 pm', '06:45 pm', '07:00 pm', '07:15 pm', '07:30 pm', '07:45 pm', '08:00 pm', '08:15 pm', '08:30 pm', '08:45 pm', '09:00 pm', '09:15 pm', '09:30 pm', '09:45 pm', '10:00 pm', '10:15 pm', '10:30 pm', '10:45 pm', '11:00 pm', '11:15 pm', '11:30 pm', '11:45 pm'];
            $scope.endTime = {
                id: 'e_time',
                value: '11:45 pm',
                options: {
                    stepsArray: $scope.endStepArray,
                    precision: 2,
                    minLimit: 0,
                    maxLimit: 100,
                    hideLimitLabels: true,
                    showSelectionBar: true,
                    onEnd: function (sliderId, modelValue) {
                        var time = $filter('convertTime')(modelValue);

                        $scope.filter.end_time = time;
                        $scope.setFilter();
                    }
                }
            };
        }
        setSliders();

        $scope.ageSlider = {
            minValue: $scope.filter.age.min,
            maxValue: $scope.filter.age.max,
            options: {
                hideLimitLabels: true,
                showSelectionBar: true,
                floor: 10,
                ceil: 70,
                step: 1,
                onEnd: function (sliderId, minValue, maxValue) {
                    $scope.filter.age.min = minValue;
                    $scope.filter.age.max = maxValue;
                    $scope.setFilter();
                }
            }
        };

        $scope.map = {
            center: {
                latitude: 56.162939,
                longitude: 10.203921
            },
            zoom: 12
        };

        $scope.options = {
            panControl: false,
            zoomControl: true,
            mapTypeControl: false,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            styles: stylesArray
        };


        clubFactory.getClub(club_id).then(
            function (response) {
                if(response.data.hours) {
                    $scope.club_cards = response.data;

                    response.data.hours = JSON.parse(response.data.hours);
                    $scope.marker = {
                        id: 1,
                        coords: {
                            latitude: response.data.latitude,
                            longitude: response.data.longitude
                        },
                        options: {
                            icon: marker_url
                        }
                    }
                    $scope.map.center.latitude = response.data.latitude;
                    $scope.map.center.longitude = response.data.longitude;
                    $scope.$broadcast('rebuild:scroll');
                }
            }
        );

        $scope.dt_options = {
            format: 'd mmmm, yyyy', // ISO formatted date
            onClose: function() {
                console.log($scope.filter.dt);
                setSliders($scope.filter.dt);
                $scope.setFilter();
            }
        }

        $scope.setFilter = function () {
            $scope.games_arr = [];
            $timeout.cancel(timer);
            $ionicLoading.show({
                template: 'Loading...'
            });
            clubFactory.getClubGames({filter: $scope.filter, id: club_id}).then(
                function (response) {
                    $scope.games_arr = response.data.rows;
                    $scope.set_timers(response.data.rows);
                    console.log('games ', response);
                    $ionicLoading.hide();
                }
            );
        }

        $scope.setFilter();

        $scope.detail = function(id) {
            $state.go('app.game-detail', {
                gameId: id,
                referral: 'app.create-match'
            });
        }
        
        $scope.joinGame = function(id){
            $state.go('app.join-game', {id_game: id});
        }

        $scope.createGame = function(id){
            GamesFactory.sendGameParams($scope.filter);
            $state.go('app.create-game', {id_gym: id});
        }


        $scope.addFavorite = function(club_id) {
          clubFactory.addFavorite(club_id).then(function (res) {
            if (res.data.status) {
              if (res.data.rows.affectedRows) {
                    $scope.club_cards.favorite = 1
                    // $scope.club_favorite_cards.push($scope.club_cards[index]);
                    // $scope.club_cards.splice(index,1);
                $
              }else {
                $scope.text = {
                  text:"Max count of faivorite",
                  description:"You can add only 5 faivorite clubs"
                }
                var dialog = ngDialog.open({
                    templateUrl: './templates/notification/show_textTplS.html',
                    className: 'ngdialog-theme-default',
                    scope: $scope
                });
              }
            }
            console.log(res);
          },function (err) {
            alert('err')
            console.log(err);

          })
        }
        $scope.$watch('filter.dt', function(){
            setSliders($scope.filter.dt)
        });

        $scope.requestMembership = function(id, index, price, name, type) {
            $scope.clubReguest = true;
            if (price > 0) {
                ngDialog.open({
                    templateUrl: './templates/club-cc-paymentTpl.html',
                    className: 'ngdialog-theme-default',
                    controller: 'club_paymentCtrl',
                    scope: $scope
                });

                $scope.$on('ngDialog.opened', function(e, $dialog) {
                    $scope.$broadcast('clubPayParams', {
                        club: id,
                        price: price,
                        credits: price,
                        name: name,
                        index: index
                    });

                });
            } else {
                if (type == 'private') {
                    clubFactory.managerRequest({
                        club_id: id
                    }).then(
                        function(response) {
                            if (response.data.status) {
                                toaster.pop({
                                    type: 'note',
                                    title: "Request Membership",
                                    body: "Sent club manager"
                                });
                                $scope.clubReguest = false;
                            } else {
                                toaster.pop({
                                    type: 'error',
                                    title: "Request Membership",
                                    body: "Not send!"
                                });
                                $scope.clubReguest = false;
                            }
                        }
                    )
                } else {
                    clubFactory.addMember({
                        club_id: id
                    }).then(
                        function(response) {
                            if (response.status == 200) {
                                $scope.club_cards[index].me_member = 1;
                                $scope.club_cards[index].members += 1;
                                $scope.clubReguest = false;
                            }
                        }
                    );
                }
            }
        };
        $scope.confirmRequest = false;
        $scope.removeMembership = function(id, index) {
            $scope.clubReguest = true;
            $scope.clubDialog = {
                id: id,
                index: index
            };
            var dialog = ngDialog.open({
                template: './templates/clubConfirmTpl.html',
                className: 'ngdialog-theme-default',
                controller: 'searchClubsCtrl',
                scope: $scope,
                showClose: true,
                width: '90%'
            });

            dialog.closePromise.then(function(data) {
                $scope.clubReguest = false;
            });
        }

        $scope.confirmCheck = function(status) {
            $scope.confirmRequest = status;
            ngDialog.closeAll();
            clubFactory.deleteMember({
                club_id: $scope.clubDialog.id
            }).then(
                function(response) {
                    if (response.status == 200) {
                        console.log($scope.club_cards[$scope.clubDialog.index]);
                        $scope.club_cards[$scope.clubDialog.index].me_member = 0;
                        $scope.club_cards[$scope.clubDialog.index].members -= 1;
                        $state.reload();
                    }
                }
            );
        }

        $scope.$on('requestClubPay', function (index) {
            $state.reload();

        })

        $scope.watchGame = function(obj, id, val) {
            obj.my_watch = val;
            console.log('val ', val);
            GamesFactory.setWatchGame({
                id: id,
                value: val
            }).then(
                function(response) {
                    console.log(response);
                }
            )
        }
    });
