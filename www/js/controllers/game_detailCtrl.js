controllers

    .controller('game_detailCtrl', function($scope, $rootScope, $location, $cookies, $state, $ionicPlatform, $filter, $timeout, GamesFactory, FiltersFactory, myGeocode, ngDialog, $stateParams, $ionicLoading) {
        var gameId = $stateParams.gameId;
        var referral_url = $stateParams.referral;

        $scope.prevState = function () {
            $state.go(referral_url,{}, {reload: true});
        };

        $scope.slider = {
            loop: true,
            effect: 'cube',
            speed: 500
        };
        $ionicLoading.show({
            template: 'Loading...'
        });

        $scope.$on("$ionicSlides.sliderInitialized", function(event, data){
            $scope.slider = data.slider;
        });

        $scope.$on("$ionicSlides.slideChangeStart", function(event, data){

        });

        $scope.$on("$ionicSlides.slideChangeEnd", function(event, data){
            $scope.activeIndex = data.slider.activeIndex;
            $scope.previousIndex = data.slider.previousIndex;
        });
        $scope.games_arr = [];
        GamesFactory.getGame(gameId).then(
            function(response){
                $scope.card = response.data.row;
                console.log($scope.card);
                $ionicLoading.hide();
            }
        );

        $scope.joinGame = function() {
            $state.go('app.join-game', {
                id_game: gameId
            });
        }
        $scope.reserveGame = function() {
            $state.go('app.reserve-game', {
                id_game: gameId
            });
        }

        $scope.createGame = function(id) {
            console.log('$scope.game_filter', $scope.game_filter);
            GamesFactory.sendGameParams($scope.game_filter);
            $state.go('app.create-game', {
                id_gym: id
            });
        }

        $scope.watchGame = function(obj, val) {
            obj.my_watch = val;
            console.log('val ', val);
            if(val){
                obj.watching += 1;
            }else{
                obj.watching -= 1;
            }
            GamesFactory.setWatchGame({
                id: gameId,
                value: val
            }).then(

                function(response) {
                    console.log(response);
                }
            )
        }
    });
