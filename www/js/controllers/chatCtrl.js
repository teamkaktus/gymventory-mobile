controllers
    .controller('chatCtrl', function ($scope, ConfigApi, userProfile, $filter, AuthFactory, Socket, $stateParams, GamesFactory, $timeout, jwtHelper, $rootScope, TeammatesFactory, MessagesFactory, ngDialog, $state, CreditsFactory) {
        $scope.id = $stateParams.id;
        $scope.user_id = jwtHelper.decodeToken(window.localStorage.getItem('token')).id;
        $scope.send = {
            room_id: $scope.id,
            message: '',
            attachments: []
        };
        $scope.url = ConfigApi.serverAddress + '/imageuploadM';
        $scope.getChatMembers = function () {
            MessagesFactory.getChatMembers($scope.id, '').then(
                function (response) {
                    $scope.teammates_arr = response.data;
                }
            );
        };
        function findWithAttr(array, attr, value) {
            for (var i = 0; i < array.length; i += 1) {
                if (array[i][attr] === value) {
                    return i;
                }
            }
            return -1;
        }

        $scope.leaveChatRoom = function (room) {
            console.log(room);
            if (room.gmid == -1) {
                MessagesFactory.leaveChatRoomM(room.id)
                    .then(
                        function (response) {
                            if (response.data.status) {
                                console.log('OK');
                                $state.go('app.messages')
                            } else {
                                $scope.dialog = ngDialog.open({
                                    template: response.data.message,
                                    plain: true,
                                    className: 'ngdialog-theme-default'
                                });
                            }
                        },
                        function (error) {
                            console.log(error);
                            $scope.dialog = ngDialog.open({
                                template: error.data,
                                plain: true,
                                className: 'ngdialog-theme-default'
                            });
                        });
            } else {
                MessagesFactory.leaveChatRoom(room.gmid)
                    .then(
                        function (response) {
                            if (response.data.status) {
                                console.log('OK');
                                $state.go('app.messages')
                            } else {
                                $scope.dialog = ngDialog.open({
                                    template: response.data.message,
                                    plain: true,
                                    className: 'ngdialog-theme-default'
                                });
                            }
                        },
                        function (error) {
                            console.log(error);
                            $scope.dialog = ngDialog.open({
                                template: error.data,
                                plain: true,
                                className: 'ngdialog-theme-default'
                            });
                        });
            }
        };

        $scope.searchChatMembers = function (search_name) {
            MessagesFactory.getChatMembers($scope.id, search_name).then(
                function (response) {
                    $scope.teammates_arr = response.data;
                }
            )
        };
        $scope.file_uploaded = function ($file, $message, $flow) {
            $message = JSON.parse($message)
            $scope.send.attachments.push($message.file.path)
        };
        $scope.data = {
            flow: new Flow()
        };
        console.log($scope.data);

        $scope.loading = false;
        $scope.loadMore = true;
        $scope.messages = []
        $scope.getChat = function () {
            if (!$scope.loading && $scope.loadMore) {
                $scope.loading = true;
                MessagesFactory.getChat($scope.id, $scope.messages.length)
                    .then(
                        function (response) {
                            if (response.data.status) {
                                $scope.room = response.data.rows[0][0]
                                if ($scope.room.club_owner != null) {
                                    $scope.room.club_owner = JSON.parse($scope.room.club_owner);
                                }
                                if ($scope.room.members_avatar != null) {
                                    $scope.room.members_avatar = JSON.parse($scope.room.members_avatar);
                                }
                                console.log($scope.room);
                                if (response.data.rows[1].length > 0) {
                                    response.data.rows[1].forEach(function (message) {
                                        if (message.attachments != null) {
                                            message.attachments = message.attachments.split(',')
                                        }
                                    })
                                    response.data.rows[1] = $filter('orderBy')(response.data.rows[1], 'date')
                                    $scope.messages = response.data.rows[1].concat($scope.messages)
                                    $scope.$broadcast('rebuild');
                                } else {
                                    $scope.loadMore = false;
                                }
                                $scope.loading = false;
                            } else {
                                $scope.loading = false;
                                $scope.dialog = ngDialog.open({
                                    template: response.data.message,
                                    plain: true,
                                    className: 'ngdialog-theme-default',
                                });
                            }
                        },
                        function (error) {
                            $scope.loading = false;
                            console.log(error);
                            $scope.dialog = ngDialog.open({
                                template: error.data,
                                plain: true,
                                className: 'ngdialog-theme-default',
                            });
                        });
            }
        };
        $scope.getChat();
        $scope.getChatMembers();
        $scope.sendMessage = function () {
            if (($scope.send.message != '') || ($scope.send.attachments.length > 0)) {
                Socket.sendMessage($scope.send)
            }
        };
        $rootScope.$on('recive-message', function (event, data) {
            if ($state.current.name == 'app.chat') {
                if (data.chat_id == $scope.id) {
                    if (data.attachments != null) {
                        data.attachments = data.attachments.split(',')
                    }
                    $scope.messages = $filter('orderBy')($scope.messages, 'date')
                    $rootScope.unreaded[$scope.id] -= 1
                    Socket.setReaded({message_id: data.message_id, chat_id: $scope.id});
                    data.viewed = 1
                    $scope.messages.push(data);
                    $scope.$broadcast('rebuild');
                }
            }
        });
        $scope.users.online = $rootScope.users.online;
        $rootScope.$on('online', function (event, data) {
            $scope.users.online = $rootScope.users.online;
            $scope.$apply()
        });
        $rootScope.$on('message-delivered', function (event, data) {
            if (data.chat_id == $scope.id) {

                if (data.attachments != null) {
                    data.attachments = data.attachments.split(',')
                }
                console.log($scope.data);
                $scope.data.flow.files = []

                $scope.send = {
                    room_id: $scope.id,
                    message: '',
                    attachments: []
                };
                $scope.messages.push(data);
                $scope.messages = $filter('orderBy')($scope.messages, 'date');
                $scope.$broadcast('rebuild');
            }


        });
        $rootScope.$on('send-message-error', function (event, data) {
            $scope.dialog = ngDialog.open({
                template: data.message,
                plain: true,
                className: 'ngdialog-theme-default'
            });
        });
        $rootScope.$on('typing-response', function (event, data) {
            if (($state.current.name == 'app.chat') && ($scope.id == data.chat_id)) {
                $scope.typingStart(data.user.id)
                console.log($scope.teammates_arr);
            }
        });
        // $rootScope.$on('unreaded-count',function (event, data) {
        //   alert(data)
        // })
        $scope.usersTyping = [];
        $scope.profileTyping = [];
        $scope.findWithAttr = function (array, attr, value) {
            for (var i = 0; i < array.length; i += 1) {
                if (array[i][attr] === value) {
                    return i;
                }
            }
            return -1;
        };
        $scope.typingStart = function (user_id) {
            if ($scope.usersTyping.indexOf(user_id) < 0) {
                var phase = $scope.$root.$$phase;
                if (phase == '$apply' || phase == '$digest') {
                    $scope.usersTyping.push(user_id);

                } else {
                    $scope.$apply(function () {
                        $scope.usersTyping.push(user_id);

                    })
                }
                $timeout(function () {
                    $scope.stopTyping(user_id)
                }, 5000);
                if (user_id == $scope.user_id) {
                    Socket.typing({chat_id: $scope.id})
                }
            }
        };
        $scope.stopTyping = function (user_id) {
            if ($scope.usersTyping.indexOf(user_id) >= 0) {
                var phase = $scope.$root.$$phase;
                if (phase == '$apply' || phase == '$digest') {
                    $scope.usersTyping.splice($scope.usersTyping.indexOf(user_id), 1);
                } else {
                    $scope.$apply(function () {
                        $scope.usersTyping.splice($scope.usersTyping.indexOf(user_id), 1);
                        ;
                    })
                }

            }
        };
        $scope.end = function () {
            $scope.$broadcast('rebuild');
        };
        $scope.remove = function (index) {
            $scope.data.flow.files.splice(index, 1)
        }
    });
