controllers
    .controller('socialConnectCtrl', function($scope,userProfile, Facebook, AuthFactory) {
        function findWithAttr(array, attr, value) {
            for (var i = 0; i < array.length; i += 1) {
                if (array[i][attr] === value) {
                    return i;
                }
            }
            return -1;
        }
        $scope.data = {
          user:null
        }
        $scope.getUserData = function () {
          userProfile.getUserData().then(
              function (response) {
                  $scope.data.user = response.data;
                  console.log($scope.data.user);
                  // $scope.sideBar = {
                  //     avatar: (userData.avatar_type == 1) ? ConfigApi.serverStatic + userData.avatar || 'img/no_avatar_vopros.jpg' : userData.avatar,
                  //     userStatus: userData.status,
                  //     firstname: userData.first_name,
                  //     lastname: userData.last_name
                  // };
              },
              function (error) {
                  console.log('error');
              }
          );
        }
        $scope.getUserData()


        $scope.user = {};

        // Defining user logged status
        $scope.logged = false;

        // And some fancy flags to display messages upon user status change
        $scope.byebye = false;
        $scope.salutation = false;

        /**
         * Watch for Facebook to be ready.
         * There's also the event that could be used
         */
        $scope.$watch(
            function() {
                return Facebook.isReady();
            },
            function(newVal) {
                if (newVal)
                    $scope.facebookReady = true;
            }
        );

        var userIsConnected = false;

        Facebook.getLoginStatus(function(response) {
            if (response.status == 'connected') {
                userIsConnected = true;
            }
        });

        /**
         * IntentLogin
         */
        $scope.IntentLogin = function() {
            if(!userIsConnected) {
                $scope.login();
            }
        };

        /**
         * Login
         */
        $scope.login = function() {
            Facebook.login(function(response) {
                if (response.status == 'connected') {
                    // $scope.logged = true;
                    $scope.me();
                }else{
                    //$ionicLoading.hide();
                    AuthFactory.removeToken();
                }
                console.log(response);

            },{
                auth_type: 'rerequest',
                scope: 'email, user_friends'
            });
        };

        /**
         * me
         */
        $scope.me = function() {
            Facebook.api('/me?fields=email,name,id', function(response) {
                /**
                 * Using $scope.$apply since this happens outside angular framework.
                 */
                $scope.$apply(function() {
                    $scope.user = response;
                    console.log(response);

                    if(response.error){
                        //$ionicLoading.hide();
                        alert(response.error.message);

                    }else {
                        AuthFactory.changeSocial(response.id,response.email)
                            .then(
                                function (response) {
                                    $scope.error = [];
                                    if (response.data.status) {
                                      alert(response.data.message);
                                    } else {
                                        alert(response.data.message);
                                        //$ionicLoading.hide();
                                    }
                                },
                                function (error) {
                                    /*$ionicLoading.hide();*/
                                    console.log('error');
                                }
                            );
                        $scope.logout();
                    }

                });

            });
        };

        $scope.logout = function() {
            Facebook.logout(function() {
                $scope.$apply(function() {
                    $scope.user   = {};
                    $scope.logged = false;
                });
            });
        }

        $scope.facebookSignIn = function() {
            /*$ionicLoading.show({
             template: 'Logging in...'
             });*/

            $scope.login();
            // Ask the permissions you need. You can learn more about
            // FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4

        };
    });
