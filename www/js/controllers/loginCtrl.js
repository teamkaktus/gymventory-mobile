controllers

    .controller('loginCtrl', function($scope, $rootScope, $state, $timeout, AuthFactory, $ionicLoading) {
        // This is the success callback from the login method
        // $ionicLoading.hide()
        $scope.data = {
            email: '',
            password: ''
        }
        $scope.error = {
            email: '',
            password: '',
            db: ''
        }
        $scope.checkUsername = function(){
            if ($scope.data.email == '') {
                $scope.error.email = 'Email can\'t be blank'
                return false;
            }else {
                $scope.error.email = ''
                return true;
            }
        }
        $scope.checkPassword = function(){
            if ($scope.data.password == '') {
                $scope.error.password = 'Password can\'t be blank'
                return false;
            }else {
                $scope.error.password = '';
                return true;
            }
        }

        $scope.loading_spiner = false;
        $scope.success_text = false;
        var timer;
        $scope.login = function () {
            cu = $scope.checkUsername();
            cp = $scope.checkPassword();
            if(cu && cp){
                $ionicLoading.show();
                AuthFactory.authenticate($scope.data.email, $scope.data.password)
                    .then(
                        function (response) {
                            console.log(response.data.status);
                            if (response.data.status) {
                                console.log(response.data.token);
                                AuthFactory.setToken(response.data.token);
                                $ionicLoading.hide();
                                $state.go('app.home', {}, {reload: true});
                            }else{
                                $ionicLoading.show(
                                    {
                                        template: response.data.message
                                    }
                                );
                                timer = $timeout(function() {
                                    $ionicLoading.hide();
                                    delete(timer);
                                }, 2000);
                            }
                        },
                        function (error) {
                            $ionicLoading.show(
                                {
                                    template: 'Invalid connection error'
                                }
                            );
                            timer = $timeout(function() {
                                $ionicLoading.hide();
								delete(timer);
                            }, 2000);
                        }
                    )
            }
        }
    })