controllers.controller('club_paymentCtrl', ['$scope', '$timeout','CreditsFactory', 'ngDialog', 'paypalFactory', '$cookies', '$state', function($scope, $timeout, CreditsFactory, ngDialog, paypalFactory, $cookies, $state) {
    $scope.cc = {
        type : 'visa',
        payment_type: 'credit_card',
        number: '4032035691990857',
        expire_month_year: '09/2021',
        cvv2: '123',
        first_name: 'Joe',
        last_name: 'Shopper',
        credits: 1,
        club: 0,
        name: '',
        index: 0
    };

    paypalFactory.removeCredits();

    $scope.loading_spiner = false;
    $scope.success_text = false;

    var timer = null;
    $scope.make_payment = function() {
        $timeout.cancel(timer);
        $scope.success_text = false;
        $scope.loading_spiner = true;

        CreditsFactory.paymentClub($scope.cc).then(
            function (response) {
                console.log(response);
                if(response.data.payment.state == 'approved') {
                    $scope.success_text = true;
                    $scope.loading_spiner = false;
                    $scope.inform_text = 'Payment successful!';
                    //$scope.$emit('resetBalance', response.data.credits);
                    $scope.$emit('requestClubPay', $scope.cc.index);
                    /*$timeout(function(){
                        $state.go('app.home');
                    }, 1000);*/
                }else if(response.data.payment.state == 'created'){
                    response.data.payment.links.forEach(function(e){
                        $cookies.remove('pp_id');
                        $cookies.put('pp_id', response.data.payment.id);
                        if(e.rel == 'approval_url' && e.method == 'REDIRECT'){
                            window.location.replace(e.href);
                        }
                    })
                }else{
                    $scope.success_text = true;
                    $scope.loading_spiner = false;
                    $scope.inform_text = 'Payment declined, try again later!';
                }
                timer = $timeout(function () {
                    $scope.success_text = false;
                    ngDialog.close();
                }, 1000);
            },
            function (error) {
                $scope.success_text = true;
                $scope.inform_text = 'There was an error when saving!';
                $scope.loading_spiner = false;
                timer = $timeout(function () {
                    $scope.success_text = false;
                }, 1000);
            }
        )
    }

    $scope.$on('clubPayParams', function (event, data) {
        $scope.cc = Object.assign($scope.cc, data);
    });
}]);
