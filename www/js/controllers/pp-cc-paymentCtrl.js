controllers.controller('pp_paymentCtrl', ['$scope', '$timeout','CreditsFactory', 'ngDialog', 'paypalFactory' , function($scope, $timeout, CreditsFactory, ngDialog, paypalFactory) {
    $scope.cc = {
        type : 'visa',
        payment_type: 'paypal',
        number: '4032035691990857',
        expire_month_year: '09/2021',
        cvv2: '123',
        first_name: 'Joe',
        last_name: 'Shopper',
        credits: 1
    };

    paypalFactory.removeCredits();

    $scope.loading_spiner = false;
    $scope.success_text = false;

    var timer = null;
    $scope.make_payment = function() {
        $timeout.cancel(timer);
        $scope.success_text = false;
        $scope.loading_spiner = true;
        CreditsFactory.buyCredits($scope.cc).then(
            function (response) {
                console.log(response);
                if(response.data.payment.state == 'approved') {
                    $scope.success_text = true;
                    $scope.loading_spiner = false;
                    $scope.inform_text = 'Payment successful!';
                    $("#balance_user").html(response.data.credits);
                    $scope.$emit('setBalance', response.data.credits);
                }else if(response.data.payment.state == 'created'){
                    paypalFactory.setCredits($scope.cc.credits);
                    response.data.payment.links.forEach(function(e){
                        if(e.rel == 'approval_url' && e.method == 'REDIRECT'){
                            window.location.replace(e.href);
                        }
                    })
                }else{
                    $scope.success_text = true;
                    $scope.loading_spiner = false;
                    $scope.inform_text = 'Payment declined, try again later!';
                }
                timer = $timeout(function () {
                    $scope.success_text = false;
                    ngDialog.close();
                }, 5000);
            },
            function (error) {
                $scope.success_text = true;
                $scope.inform_text = 'There was an error when saving!';
                $scope.loading_spiner = false;
                timer = $timeout(function () {
                    $scope.success_text = false;
                }, 5000);
            }
        )
    }
}]);