controllers
    .controller('bookingCtrl', function ($scope, $rootScope, $state, $stateParams, $filter, $timeout, GamesFactory, clubFactory) {
        var id_t = $stateParams.t_id;
        $scope.game_status = 'active';
        var timer;
        $scope.dates_arr = []
        $scope.set_timers = function (data) {
            var date_tmp = '';
            if (data) {
                data.forEach(function (el, i, arr) {
                    if (date_tmp == '') {
                        if (el.date && el.game_id) {
                            $scope.dates_arr[el.game_id] = el.date;
                            date_tmp = el.date;
                        } else {
                            $scope.dates_arr[0] = $scope.filter.dt;
                            date_tmp = $scope.filter.dt;
                        }
                    }
                    if (el.date) {
                        if (date_tmp != el.date && el.date && el.game_id) {
                            $scope.dates_arr[el.game_id] = el.date;
                            date_tmp = el.date;
                        }
                        var startDate = new Date(el.date);
                        var dd = startDate.getDate();
                        var mm = startDate.getMonth() + 1; //January is 0!
                        var yyyy = startDate.getFullYear();
                        var endtime = yyyy + '-' + mm + '-' + dd + ' ' + el.start_time.toString().replace('.', ':') + ':00';


                        function getTimeRemaining(endtime) {

                            var t = Date.parse(endtime) - Date.parse(new Date());
                            var seconds = Math.floor((t / 1000) % 60);
                            var minutes = Math.floor((t / 1000 / 60) % 60);
                            var hours = Math.floor(t / (1000 * 60 * 60));


                            return {
                                'total': t,
                                'hours': hours,
                                'minutes': minutes,
                                'seconds': seconds
                            };
                        }

                        $scope.clock = [];
                        $scope.clock[el.game_id] = "00:00:00";
                        var tick = function () {
                            var t = getTimeRemaining(endtime);
                            $scope.clock[el.game_id] = t.hours + ':' + t.minutes + ':' + t.seconds;
                            timer = $timeout(tick, 1000); // reset the timer
                        }
                        timer = $timeout(tick, 1000);
                    }
                })
            }
        }

        $scope.en_time = $filter('filterClockSlider')((new Date).getHours());
        $scope.tab_club_page = "Gym";

        $scope.filter = {
            club_id: id_t,
            sport: {
                1: false,
                2: false,
                3: false,
                4: false,
                5: false,
                6: false,
                7: false,
                8: false,
                9: false,
                10: false,
                11: false,
                12: false,
                13: false,
                14: false,
                15: false,
                16: false
            },
            access: 'All',
            start_time: 0,
            end_time: 23.45,

            sort: 'Soonest',
            age: {
                min: 20,
                max: 60
            },
            dt: null,
            gender: 'All'
        };
        //console.log("online", $rootScope.users.online);

        $scope.clearFil = function () {
            $scope.filter.sport = {
                1: false,
                2: false,
                3: false,
                4: false,
                5: false,
                6: false,
                7: false,
                8: false,
                9: false,
                10: false,
                11: false,
                12: false,
                13: false,
                14: false,
                15: false,
                16: false
            };
            $scope.filter.access = 'All';
            $scope.filter.start_time = 0;
            $scope.filter.end_time = 23.45;
            $scope.filter.age = {
                min: 20,
                max: 60
            };
            $scope.filter.gender = 'All';
            $scope.filter.sort = 'Soonest';


            $scope.filter.dt = null; //new Date();

            setSliders();
            $scope.findGames();
        };

        $scope.myPosition = true;
        $scope.myAddress = true;
        $scope.distanceDisabled = true;
        $scope.distanceClass = 'disabled';

        $scope.club_cards = [];


        if (!navigator.geolocation) {
            $scope.myPosition = false;
            return false;
        }

        var set_time = function (arg, arr, date_val) {
            console.log('date_val', date_val);
            var to_hour = (new Date).getHours();
            var limit = 0,
                start = "08:00 am",
                end = "11:00 pm";
            switch (arg) {
                case "start": {
                    if ((new Date).getMonth() == (date_val).getMonth() && (new Date).getDate() == (date_val).getDate()) {
                        limit = arr.indexOf($filter('filterClockSlider')(to_hour + 1));
                        start = $filter('filterClockSlider')(to_hour + 1);
                    } else {
                        limit = 0;
                    }

                    return {
                        start: start,
                        limit: limit
                    };
                }
                case "end": {
                    if ((new Date).getMonth() == ($scope.filter.dt).getMonth() && (new Date).getDate() == ($scope.filter.dt).getDate()) {
                        limit = arr.indexOf($filter('filterClockSlider')(to_hour + 2));
                        //end = $filter('filterClockSlider')(23);
                    } else {
                        limit = 0;
                    }
                    return {
                        end: end,
                        limit: limit
                    };
                }
            }
        }

        var setSliders = function (date_val) {

            $scope.startStepArray = ['12:00 am', '12:15 am', '12:30 am', '12:45 am', '01:00 am', '01:15 am', '01:30 am', '01:45 am', '02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 pm', '12:15 pm', '12:30 pm', '12:45 pm', '01:00 pm', '01:15 pm', '01:30 pm', '01:45 pm', '02:00 pm', '02:15 pm', '02:30 pm', '02:45 pm', '03:00 pm', '03:15 pm', '03:30 pm', '03:45 pm', '04:00 pm', '04:15 pm', '04:30 pm', '04:45 pm', '05:00 pm', '05:15 pm', '05:30 pm', '05:45 pm', '06:00 pm', '06:15 pm', '06:30 pm', '06:45 pm', '07:00 pm', '07:15 pm', '07:30 pm', '07:45 pm', '08:00 pm', '08:15 pm', '08:30 pm', '08:45 pm', '09:00 pm', '09:15 pm', '09:30 pm', '09:45 pm', '10:00 pm', '10:15 pm', '10:30 pm', '10:45 pm'];
            $scope.startTime = {
                id: 's_time',
                value: "12:00 am",
                options: {
                    stepsArray: $scope.startStepArray,
                    precision: 2,
                    minLimit: 0,
                    maxLimit: 100,
                    hideLimitLabels: true,
                    showSelectionBar: true,
                    draggableRange: true,
                    onEnd: function (sliderId, modelValue) {
                        var time = $filter('convertTime')(modelValue);
                        if (time >= 24) {
                            time -= 24
                        }

                        $scope.game_filter.start_time = time;
                        $scope.en_time = modelValue;
                        $scope.findGames();


                    }
                }
            };
            $scope.endStepArray = ['01:00 am', '01:15 am', '01:30 am', '01:45 am', '02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 pm', '12:15 pm', '12:30 pm', '12:45 pm', '01:00 pm', '01:15 pm', '01:30 pm', '01:45 pm', '02:00 pm', '02:15 pm', '02:30 pm', '02:45 pm', '03:00 pm', '03:15 pm', '03:30 pm', '03:45 pm', '04:00 pm', '04:15 pm', '04:30 pm', '04:45 pm', '05:00 pm', '05:15 pm', '05:30 pm', '05:45 pm', '06:00 pm', '06:15 pm', '06:30 pm', '06:45 pm', '07:00 pm', '07:15 pm', '07:30 pm', '07:45 pm', '08:00 pm', '08:15 pm', '08:30 pm', '08:45 pm', '09:00 pm', '09:15 pm', '09:30 pm', '09:45 pm', '10:00 pm', '10:15 pm', '10:30 pm', '10:45 pm', '11:00 pm', '11:15 pm', '11:30 pm', '11:45 pm'];
            $scope.endTime = {
                id: 'e_time',
                value: '11:45 pm',
                options: {
                    stepsArray: $scope.endStepArray,
                    precision: 2,
                    minLimit: 0,
                    maxLimit: 100,
                    hideLimitLabels: true,
                    showSelectionBar: true,
                    onEnd: function (sliderId, modelValue) {
                        var time = $filter('convertTime')(modelValue);

                        $scope.game_filter.end_time = time;
                        $scope.findGames();
                    }
                }
            };
        }
        setSliders();

        $scope.ageSlider = {
            minValue: $scope.filter.age.min,
            maxValue: $scope.filter.age.max,
            options: {
                hideLimitLabels: true,
                showSelectionBar: true,
                floor: 10,
                ceil: 70,
                step: 1,
                onEnd: function (sliderId, minValue, maxValue) {
                    $scope.filter.age.min = minValue;
                    $scope.filter.age.max = maxValue;
                    $scope.setFilter();
                }
            }
        };

        $scope.map = {
            center: {
                latitude: 56.162939,
                longitude: 10.203921
            },
            zoom: 12
        };

        $scope.options = {
            panControl: false,
            zoomControl: true,
            mapTypeControl: false,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            styles: stylesArray
        };

        $scope.dt_options = {
            format: 'd mmmm, yyyy', // ISO formatted date
            onClose: function() {
                console.log($scope.filter.dt);
                setSliders($scope.filter.dt);
                $scope.findGames();
            }
        }


        $scope.limit_view = 10000;
        $scope.findGames = function () {
            console.log('here');
            $timeout.cancel(timer);
            $scope.filter.limit_start = 0;
            $scope.filter.limit_end = parseInt($scope.limit_view);
            $scope.games_arr = [];
            $scope.$broadcast('rebuild:scroll');
            console.log('distanceDisabled ', !$scope.distanceDisabled);
            console.log('filter ', $scope.filter);
            GamesFactory.findBookingGames($scope.filter).then(
                function (response) {
                    $scope.$broadcast('rebuild:scroll');
                    console.log(response);
                    $scope.games_arr = response.data.rows;
                    console.log($scope.games_arr);
                    $scope.set_timers(response.data.rows);
                    $scope.urlBase = GamesFactory.urlBase;
                }
            )
        }
        $scope.findGames();

        $scope.detail = function (id) {
            $state.go('app.game-detail', {gameId: id, referral: 'app.booking'});
        }

        $scope.mustStart = function ($event, id) {
            GamesFactory.mustStartGame({game_id: id}).then(
                function (response) {
                    angular.element($event.target).addClass('disable-btn').attr("disabled", "disabled");
                }
            )
        }

    });
