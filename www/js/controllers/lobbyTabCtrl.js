controllers

    .controller('lobbyTabCtrl', function($scope, $rootScope, $location, $cookies, $state, $ionicPlatform, $filter, $timeout, GamesFactory, FiltersFactory, myGeocode, ngDialog, $stateParams, $ionicLoading) {
        var activeTab = $stateParams.tab;
        $scope.slider = {
            loop: true,
            effect: 'cube',
            speed: 500
        };
        $ionicLoading.show({
            template: 'Loading...'
        });

        $scope.$on("$ionicSlides.sliderInitialized", function(event, data){
            $scope.slider = data.slider;
        });

        $scope.$on("$ionicSlides.slideChangeStart", function(event, data){

        });

        $scope.$on("$ionicSlides.slideChangeEnd", function(event, data){
            $scope.activeIndex = data.slider.activeIndex;
            $scope.previousIndex = data.slider.previousIndex;
        });
        $scope.showPager = false;

    $scope.tab_lobby = 'all';
    $scope.configSBar = {
        autoHideScrollbar: false,
        theme: 'light',
        advanced: {
            updateOnContentResize: true
        },
        setHeight: 200,
        scrollInertia: 0
    };

    console.log(activeTab);
    switch (activeTab) {
        case 'all': {
            $scope.games_arr = [];
            GamesFactory.getGames($scope.game_filter).then(
                function (response) {
                    $scope.games_arr = $scope.games_arr.concat(response.data.rows);
                    $scope.games_arr = $filter('orderBy')($scope.games_arr, 'date')
                }
            )
            GamesFactory.getGamesReserved($scope.game_filter).then(
                function (response) {
                    $scope.games_arr = $scope.games_arr.concat(response.data.rows);
                    $scope.games_arr = $filter('orderBy')($scope.games_arr, 'date')
                }
            )
            GamesFactory.getGamesWatch($scope.game_filter).then(
                function (response) {
                    $scope.games_arr = $scope.games_arr.concat(response.data.rows);
                    console.log(response.data.rows);
                    $scope.games_arr = $filter('orderBy')($scope.games_arr, 'date')
                }
            )
            GamesFactory.getGamesInvites($scope.game_filter).then(
                function (response) {
                    $scope.games_arr = $scope.games_arr.concat(response.data.rows);
                    $scope.games_arr = $filter('orderBy')($scope.games_arr, 'date')
                }
            )
            break;
        }
        case 'reserve': {
            $scope.games_arr = [];
            GamesFactory.getGamesReserved($scope.game_filter).then(
                function (response) {
                    $scope.games_arr = response.data.rows;
                    $ionicLoading.hide();
                }
            )
            $scope.btnClick = true;
            break;
        }
        case 'watch': {
            $scope.games_arr = [];
            GamesFactory.getGamesWatch($scope.game_filter).then(
                function (response) {
                    $scope.games_arr = response.data.rows;
                    $ionicLoading.hide();
                }
            )
            $scope.btnClick = true;
            break;
        }
        case 'joined': {
            $scope.games_arr = [];
            GamesFactory.getGames($scope.game_filter).then(
                function (response) {
                    $scope.games_arr = response.data.rows;
                    $ionicLoading.hide();
                    console.log($scope.games_arr);
                }
            )
            break;
        }
        case 'invite': {
            $scope.games_arr = [];
            GamesFactory.getGamesInvites($scope.game_filter).then(
                function (response) {
                    $scope.games_arr = response.data.rows;
                    $ionicLoading.hide();
                }
            )
            $scope.btnClick = true;
            break;
        }
    }

    $scope.payReserve = function (card) {
        $scope.game_id = card.id;
        $scope.club_user = card.club_user;
        $scope.game_price = (card.global_members * card.price);

        $scope.dialog = ngDialog.open({
            templateUrl: './templates/credits-paymentTpl.html',
            className: 'ngdialog-theme-default',
            controller: 'paymentReserveCtrl',
            scope: $scope
        });

        $scope.$on('ngDialog.opened', function (e, $dialog) {
            $("#game-cost").html(card.global_members * card.price);
        });

    };

    $scope.watchGame = function (item, id, val) {
        item.watching = val;
        console.log('val ', val);
        GamesFactory.setWatchGame({
            id: id,
            value: val
        }).then(
            function (response) {
                console.log(response);
            }
        )
    };

    $scope.joinGame = function (id) {
        console.log(id);
        $state.go('app.join-game', {
            id_game: id
        }, {reload:true});
    }
});
