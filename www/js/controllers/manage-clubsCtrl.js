controllers.controller('manage-clubsCtrl', function ($scope, $rootScope, clubFactory, $state) {

    $rootScope.club = {
        create: false
    };

    $scope.check_club = false;
    $scope.inform_text = false;

    clubFactory.countClub().then(
        function (response) {
            if (parseInt(response.data.count) >= 1) {
                $rootScope.club.create = false;
                console.log("count club", $rootScope.club.create)
            } else {
                $rootScope.club.create = true;
                if($rootScope.club.create){
                    $state.go('app.createMyClub');
                }
            }
        },
        function (error) {
            console.log(error);
        }
    );


    $scope.o_hours = [];

    function get24clock() {
        var array = [];
        for (var i = 1; i <= 24; i++) {
            array.push(i);
        }
        return array;
    }

    $scope.o_hours = get24clock();

    $scope.map = {
        center: {
            latitude: 56.162939,
            longitude: 10.203921
        },
        zoom: 12
    };

    $scope.options = {
        panControl: false,
        zoomControl: true,
        mapTypeControl: false,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        styles: stylesArray
    };
    $scope.club = {
        latitude: '',
        longitude: ''
    };

    $scope.marker = {
        id: 0,
        coords: {
            latitude: 56.162939,
            longitude: 10.203921
        },
        options: {
            draggable: true,
            icon: marker_url
        },
        events: {
            dragend: function (marker, eventName, args) {
                var lat = marker.getPosition().lat();
                var lon = marker.getPosition().lng();

                $scope.club.latitude = lat;
                $scope.club.longitude = lon;

                $scope.marker.options = {
                    draggable: true,
                    labelContent: "",
                    labelAnchor: "100 0",
                    labelClass: "marker-labels",
                    icon: marker_url
                };
            }
        }
    };

    $scope.$watchCollection("marker.coords", function (newVal, oldVal) {
        $scope.map.center.latitude = $scope.marker.coords.latitude;
        $scope.map.center.longitude = $scope.marker.coords.longitude;

        if (_.isEqual(newVal, oldVal))
            return;
        return true;
    });

    clubFactory.getClubData().then(
        function (response) {
            console.log('response');
            if(!response.data){
                $state.go('app.home');
            }
            $scope.gym = {
              id: response.data.club_id
            };
            $scope.club = {
                id: response.data.club_id,
                name: response.data.name,
                type: response.data.type,
                address: response.data.address,
                price: response.data.price,
                latitude: response.data.latitude,
                longitude: response.data.longitude,
                phone: response.data.phone,
                website: response.data.website,
                email: response.data.email,
                fax: response.data.fax,
                day: {
                    mon: {
                        start: 8,
                        end: 22
                    },
                    tue: {
                        start: 8,
                        end: 22
                    },
                    wed: {
                        start: 8,
                        end: 22
                    },
                    thu: {
                        start: 8,
                        end: 22
                    },
                    fri: {
                        start: 8,
                        end: 22
                    },
                    sat: {
                        start: 8,
                        end: 22
                    },
                    sun: {
                        start: 8,
                        end: 22
                    }
                }
            };

            $scope.day = {mon: true, tue: true, wed: true, thu: true, fri: true, sat: false, sun: false};
            if (response.data.hours) {
                $scope.day = {mon: false, tue: false, wed: false, thu: false, fri: false, sat: false, sun: false};
                var work_hours = JSON.parse(response.data.hours);
                for (var key in work_hours) {
                    $scope.day[key] = true;
                    $scope.club.day[key] = work_hours[key];
                }
            }

            $scope.map.center.latitude = response.data.latitude;
            $scope.map.center.longitude = response.data.longitude;

            $scope.marker.coords.latitude = response.data.latitude;
            $scope.marker.coords.longitude = response.data.longitude;

            if($scope.club != null){
                $scope.inform_text = false;
                $scope.check_club = true;
            }


        },
        function (error) {
            console.log('error');
            $scope.check_club = false;
            $scope.inform_text = true;
            console.log(error);
        }
    );

    console.log('check_club', $scope.check_club);
    console.log('inform_text', $scope.inform_text);


    $scope.changeAddress = function () {
        clubFactory.getAddressCoordinate($scope.club.address).then(
            function (response) {
                var coords = response.data.results[0].geometry.location;

                $scope.map.center.latitude = coords.lat;
                $scope.map.center.longitude = coords.lng;

                $scope.marker.coords.latitude = coords.lat;
                $scope.marker.coords.longitude = coords.lng;

                $scope.club.latitude = coords.lat;
                $scope.club.longitude = coords.lng;
            },
            function (error) {
            }
        )
    };

    $scope.editClub = function (data) {
        if (data.day) {
            for (var key in $scope.day) {
                if ($scope.day[key] == false) {
                    if (data.day[key])
                        delete(data.day[key]);
                }
            }
        }
        if(data.type == 'public'){
            data.price = 0;
        }
        clubFactory.updateClubData(data).then(
            function (response) {
                $state.reload();
                console.log(response);
            },
            function (error) {
                console.log(error);
            }
        )
    }
});