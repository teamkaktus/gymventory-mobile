controllers

    .controller('signupCtrl', function($scope, $state, ConfigApi, AuthFactory, ngDialog, $ionicLoading, $timeout, $filter) {
        $scope.loading_spiner = false;

        $scope.daysInMonth = function(month, year) {
            return new Date(year, month, 0).getDate();
        };

        $scope.heightData = {
            ft: "1'",
            in: '5"'
        };

        $scope.userData = {
            email: '',
            first_name: '',
            last_name: '',
            password: '',
            img: 'img/no-img.jpg',
            tel: '',
            telCode: '+1',
            gender: 'Female',
            height: $scope.heightData.ft + $scope.heightData.in,
            birth: ''
        };
        $scope.dob = {
            day: [],
            dayS: 1,
            month: [],
            monthS: 1,
            year: [],
            yearS: 1990
        };
        $scope.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        $scope.error = {
            email: '',
            first_name: '',
            last_name: '',
            password: '',
            img: '',
            gender: '',
            height: '',
            birth: ''
        };
        AuthFactory.telephone()
            .then(
                function(response) {
                    console.log('telephone ', response);
                    $scope.telephone = response.data;
                },
                function(error) {
                    console.log('error');
                }
            );
        $scope.dateBuild = function() {
            $scope.dob.day = []
            for (var i = 1; i <= $scope.daysInMonth($scope.dob.monthS, $scope.dob.yearS); i++) {
                $scope.dob.day.push(i);
            }
            if (!$scope.dob.month.length) {
                for (var i = 1; i <= 12; i++) {
                    $scope.dob.month.push(i);
                }
            }
            if (!$scope.dob.year.length) {
                for (var i = 1950; i <= new Date().getFullYear(); i++) {
                    $scope.dob.year.push(i);
                }
            }
            $scope.userData.dob = $scope.dob.monthS + '-' + $scope.dob.dayS + '-' + $scope.dob.yearS
        };
        $scope.dateBuild();
        $scope.checkForm = function(form) {
            if (form.$invalid) {
                angular.forEach(form.$error, function(field) {
                    angular.forEach(field, function(errorField) {
                        errorField.$setTouched();
                    })
                });
                return false;
            } else {
                return true;
            }
        };
        $scope.signUp = function(form) {
            if ($scope.checkForm(form)) {
                $ionicLoading.show();
                console.log($scope.userData);
                AuthFactory.signUp($scope.userData).then(
                    function(response) {
                        if (response.data.status) {
                            AuthFactory.authenticate($scope.userData.email, $scope.userData.password).then(
                                function(response) {
                                    if (response.data.status) {
                                        console.log(response.data);
                                        $ionicLoading.hide();
                                        AuthFactory.setToken(response.data.token);
                                        $state.go('app.home', {}, {
                                            reload: true
                                        });
                                    } else {
                                        console.log(response.data);
                                        $ionicLoading.hide();
                                    }
                                },
                                function(error) {
                                    console.log(response.data);
                                    $ionicLoading.hide();
                                });
                        } else {
                            if (typeof response.data.errors != 'undefined') {
                                console.log(response.data);
                                $ionicLoading.show({
                                    template: '<i class="ion-android-warning"></i> ' + response.data.errors[0].message
                                });
                                timer = $timeout(function() {
                                    $ionicLoading.hide();
                                    delete(timer);
                                }, 2000);
                                // angular.forEach(response.data.errors, function (error) {
                                //     $scope.error[error.field] = error.message;
                                // })
                            }
                        }
                    },
                    function(error) {
                        console.log(error);
                        $ionicLoading.hide();
                    }
                );
            }
        };
        $scope.file_uploaded = function($file, $message, $flow) {
            $scope.userData.img = JSON.parse($message)['file'].path;
        };
        // $scope.$watch('userData.tel', function(value, oldValue) {
        //     value = String(value);
        //     var number = value.replace(/[^0-9]+/g, '');
        //     $scope.userData.tel = $filter('phonenumber')(number);
        // });
    });
