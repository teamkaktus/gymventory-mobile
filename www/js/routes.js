angular.module('app.routes', [])

    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('app', {
                cache: false,
                templateUrl: 'templates/left-sidebarTpl.html',
                controller: 'asideCtrl',
                abstract:true
            })
            .state('app.home', {
                cache: false,
                url: '/',
                views: {
                  'app': {
                    templateUrl: 'templates/lobbyTpl.html',
                    controller: 'lobbyCtrl'
                  }
                }
            })
            .state('app.lobby-tab', {
                cache: false,
                url: '/lobby-tab/:tab',
                views: {
                    'app': {
                        templateUrl: 'templates/lobbyTabTpl.html',
                        controller: 'lobbyTabCtrl'
                    }
                }
            })
            .state('app.game-detail', {
                cache: false,
                url: '/game-detail/:gameId/:referral',
                views: {
                    'app': {
                        templateUrl: 'templates/game_detailTpl.html',
                        controller: 'game_detailCtrl'
                    }
                }
            })
            .state('app.notifications', {
                cache: false,
                url: '/notifications',
                views: {
                    'app': {
                        templateUrl: 'templates/notificationsTpl.html',
                        controller: 'notificationsCtrl'
                    }
                }
            })
            .state('app.b_notifications', {
                cache: false,
                url: '/business-notifications',
                views: {
                    'app': {
                        templateUrl: 'templates/notification/business_notificationTpl.html',
                        controller: 'b_notificationsCtrl'
                    }
                }
            })
            .state('app.paypal', {
                cache: false,
                url: '/paypal',
                views: {
                    'app': {
                        templateUrl: 'templates/paypal_callbackTpl.html',
                        controller: 'paypal_callbackCtrl'
                    }
                }
            })
            .state('app.profile', {
                cache: false,
                url: '/profile',
                views: {
                    'app': {
                        templateUrl: 'templates/edit-profileTpl.html',
                        controller: 'edit-profileCtrl'
                    }
                }
            })
            .state('app.withdrawal', {
                cache: false,
                url: '/withdrawal',
                views: {
                    'app': {
                        templateUrl: 'templates/withdrawalTpl.html',
                        controller: 'withdrawalCtrl'
                    }
                }
            })
            .state('app.transactions', {
                cache: false,
                url: '/transactions',
                views: {
                    'app': {
                        templateUrl: 'templates/transactionsTpl.html',
                        controller: 'transactionsCtrl'
                    }
                }
            })
            .state('app.manage_clubs', {
                cache: false,
                url: '/manage_clubs',
                views: {
                    'app': {
                        templateUrl: 'templates/manage-clubs-gymsTpl.html',
                        controller: 'manage-clubsCtrl'
                    }
                }
            })
            .state('app.search', {
                cache: false,
                url: '/search',
                views: {
                    'app': {
                        templateUrl: 'templates/searchClubsTpl.html',
                        controller: 'searchClubsCtrl'
                    }
                }
            })
            .state('app.last-visited', {
                cache: false,
                url: '/last-visited',
                views: {
                    'app': {
                        templateUrl: 'templates/last-visitedClubsTpl.html',
                        controller: 'last-visitedClubsCtrl'
                    }
                }
            })
            .state('app.create-match', {
                cache: false,
                url: '/create-match',
                views: {
                    'app': {
                        templateUrl: 'templates/create-matchTpl.html',
                        controller: 'create-matchCtrl'
                    }
                }
            })

            .state('app.messages', {
                cache: false,
                url: '/massages',
                views: {
                    'app': {
                        templateUrl: 'templates/messagesTpl.html',
                        controller: 'messagesCtrl'
                    }
                }
            })
            .state('app.chat', {
                cache: false,
                url: '/massages/:id',
                views: {
                    'app': {
                        templateUrl: 'templates/chatTpl.html',
                        controller: 'chatCtrl'
                    }
                }
            })

            .state('app.create-class', {
                cache: false,
                url: '/create-class',
                views: {
                    'app': {
                        templateUrl: 'templates/create-classTpl.html',
                        controller: 'create-classCtrl'
                    }
                }
            })

            .state('app.join-game', {
                cache: false,
                url: '/join-game/:id_game/',
                views: {
                    'app': {
                        templateUrl: 'templates/join-gameTpl.html',
                        controller: 'join-gameCtrl'
                    }
                }
            })
            .state('app.reserve-game', {
                cache: false,
                url: '/reserve-game/:id_game/',
                views: {
                    'app': {
                        templateUrl: 'templates/reserve-gameTpl.html',
                        controller: 'reserve-gameCtrl'
                    }
                }
            })
            .state('app.create-game', {
                cache: false,
                url: '/create-game/:id_gym/',
                params:{
                    club_params: {
                    }
                },
                views: {
                    'app': {
                        templateUrl: 'templates/create-gameTpl.html',
                        controller: 'create-gameCtrl'
                    }
                }
            })
            .state('app.teammates', {
                cache: false,
                url: '/teammates',
                views: {
                    'app': {
                        templateUrl: 'templates/teammatesTpl.html',
                        controller: 'teammatesCtrl'
                    }
                }
            })
            .state('app.club-page', {
                cache: false,
                url: '/club-page/:club_id',
                views: {
                    'app': {
                        templateUrl: 'templates/club-pageTpl.html',
                        controller: 'club-pageCtrl'
                    }
                }
            })
            .state('app.teammate-page', {
                cache: false,
                url: '/teammate-page/:t_id',
                views: {
                    'app': {
                        templateUrl: 'templates/teammate-pageTpl.html',
                        controller: 'teammate-pageCtrl'
                    }
                }
            })
            .state('app.user', {
                cache: false,
                url: '/user/:t_id',
                views: {
                    'app': {
                        templateUrl: 'templates/profile-pageTpl.html',
                        controller: 'profile-pageCtrl'
                    }
                }
            })
            .state('app.members', {
                cache: false,
                url: '/members',
                views: {
                    'app': {
                        templateUrl: 'templates/membersTpl.html',
                        controller: 'membersCtrl'
                    }
                }
            })
            .state('app.booking', {
                cache: false,
                url: '/booking',
                views: {
                    'app': {
                        templateUrl: 'templates/bookingTpl.html',
                        controller: 'bookingCtrl'
                    }
                }
            })
            .state('app.clubRequest', {
                cache: false,
                url: '/club-request',
                views: {
                    'app': {
                        templateUrl: 'templates/notification/business_club_requestTpl.html',
                        controller: 'b_club_requestCtrl'
                    }
                }
            })
            .state('app.createMyClub', {
                cache: false,
                url: '/create-my-club',
                views: {
                    'app': {
                        templateUrl: 'templates/find_to_create_clubTpl.html',
                        controller: 'findToCreateClubCtrl'
                    }
                }
            })
            .state('app.clubCourts', {
                cache: false,
                url: '/club-courts',
                views: {
                    'app': {
                        templateUrl: 'templates/club-courtsTpl.html',
                        controller: 'club-courtsCtrl'
                    }
                }
            })
            .state('app.courtsItem', {
                cache: false,
                url: '/club-courts/:id',
                views: {
                    'app': {
                        templateUrl: 'templates/club-courts_itemTpl.html',
                        controller: 'club-courts_itemCtrl'
                    }
                }
            })
            .state('app.clubClasses', {
                cache: false,
                url: '/club-classes',
                views: {
                    'app': {
                        templateUrl: 'templates/club-classesTpl.html',
                        controller: 'club-classesCtrl'
                    }
                }
            })
            .state('app.classesItem', {
                cache: false,
                url: '/club-classes/:id',
                views: {
                    'app': {
                        templateUrl: 'templates/club-classes_itemTpl.html',
                        controller: 'club-classes_itemCtrl'
                    }
                }
            })
            .state('app.socialConnect', {
                cache: false,
                url: '/social-connect',
                views: {
                    'app': {
                        templateUrl: 'templates/social-connect.html',
                        controller: 'socialConnectCtrl'
                    }
                }
            })
            .state('start', {
                cache: false,
                url: '/start',
                templateUrl: 'templates/startTpl.html'
            })
            .state('signin', {
                cache: false,
                url: '/signin',
                templateUrl: 'templates/signinTpl.html',
                controller: 'signinCtrl'
            })
            .state('signup', {
                cache: false,
                url: '/signup',
                templateUrl: 'templates/signupTpl.html',
                controller: 'signupCtrl'
            });
        $urlRouterProvider.otherwise('/');
    }]);
