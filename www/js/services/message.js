services

    .factory('MessagesFactory', ['$http', '$state', 'ConfigApi', 'AuthFactory', function ($http, $state, ConfigApi, AuthFactory) {
        var urlBase = ConfigApi.serverAddress + '/';
        var MessagesFactory = {};

        MessagesFactory.urlBase = ConfigApi.serverStatic;

        // GamesFactory.findGames = function(data){
        //     return $http.post(urlBase + 'findGames', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        // };
        MessagesFactory.getChatRooms = function (data) {
            return $http.post(urlBase + 'rooms',data,{headers:{'X-Access-Token': AuthFactory.getToken()}})
        }
        MessagesFactory.getChat = function (id, from) {
            return $http.get(urlBase + 'rooms/'+id + '?from=' + from,{headers:{'X-Access-Token': AuthFactory.getToken()}})
        }
        MessagesFactory.acceptChatRoom = function (chat_id) {
            return $http.post(urlBase + 'rooms/'+ chat_id,{token: AuthFactory.getToken()})
        }
        MessagesFactory.acceptChatRoomM = function (gid) {
            return $http.post(urlBase + 'roomsm/'+ gid,{token: AuthFactory.getToken()})
        }
        MessagesFactory.leaveChatRoom = function (chat_id) {
            return $http.delete(urlBase + 'rooms/'+ chat_id,{headers:{'X-Access-Token': AuthFactory.getToken()}})
        }
        MessagesFactory.leaveChatRoomM = function (gid) {
            return $http.delete(urlBase + 'roomsm/'+ gid,{headers:{'X-Access-Token': AuthFactory.getToken()}})
        }
        MessagesFactory.getChatMembers = function(chat_id,name){
            return $http.get(urlBase + 'rooms/'+ chat_id +'/members?name='+name, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };
        return MessagesFactory;
    }])
