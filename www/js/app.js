var app = angular.module('gumApp', ['ionic', 'slick', 'angular-datepicker', 'dcbImgFallback', 'ngCordova', 'ui.router', 'app.routes', 'ngCookies', 'app.services', 'app.configs', 'app.controllers', 'angular-jwt', 'app.directives', 'facebook', 'angular-click-outside', 'ngDialog', 'flow', 'ui.mask', 'google.places', 'uiGmapgoogle-maps', 'rzModule', 'ui.bootstrap', 'ngScrollbar', 'infinite-scroll', 'wysiwyg.module', 'ngSanitize', 'toaster', 'ngAnimate'])

    .config(function(FacebookProvider, flowFactoryProvider, $ionicConfigProvider, ConfigApi, uiGmapGoogleMapApiProvider) {

        var myAppId = ConfigApi.facebookLogin;
        FacebookProvider.init(myAppId);
        $ionicConfigProvider.views.maxCache(5);
        flowFactoryProvider.defaults = {
            target: ConfigApi.serverAddress + '/imageupload',
            permanentErrors: [404, 500, 501],
            maxChunkRetries: 1,
            chunkRetryInterval: 5000,
            simultaneousUploads: 4,
            testChunks: false,
            singleFile: true
        };

        new WOW({
            boxClass: 'wow', // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 0, // distance to the element when triggering the animation (default is 0)
            mobile: true, // trigger animations on mobile devices (default is true)
            live: true, // act on asynchronously loaded content (default is true)
            callback: function(box) {
                // the callback is fired every time an animation is started
                // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: null // optional scroll container selector, otherwise use window
        }).init();

        uiGmapGoogleMapApiProvider.configure({
            china: true
        });
    })
    .run(function($ionicPlatform, $rootScope, $state, $timeout, AuthFactory) {
        $ionicPlatform.ready(function() {
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
        $rootScope.users = {
            online: []
        };
        $rootScope.unreaded = [];
        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams) {
                $rootScope.currState = toState.name;
                console.log(['start', 'signin', 'signup'].indexOf(toState.name), toState.name);
                if (['start', 'signin', 'signup'].indexOf(toState.name) == -1) {
                    if (!AuthFactory.authenticated()) {
                        $timeout(function() {
                            $state.go('start');
                        });
                    }
                } else {
                    if (AuthFactory.authenticated()) {
                        $state.go('app.home');
                    } else {
                        $timeout(function() {
                            $state.go(toState.name);
                        });
                    }
                }
            });
    });

app.filter('unique', function() {
        return function(collection, keyname) {
            var output = [],
                keys = [];

            angular.forEach(collection, function(item) {
                var key = item[keyname];
                if (keys.indexOf(key) === -1) {
                    keys.push(key);
                    output.push(item);
                }
            });

            return output;
        };
    })
    .filter('day', function($filter) {
        var mappings = {
            "0": "Today",
            "+1": "Tomorrow",
            "-1": "Yesterday",
        };
        return function(entry) {
            var d1 = moment.utc(entry);
            var d2 = moment.utc();
            var mappedValue = mappings[d1.diff(d2, 'days')];
            if (typeof mappedValue != 'undefined') {
                return mappedValue + ' '
            } else {
                return d1.format("dddd Do MMM");
            }
        }
    });

app.filter('day_notification', function($filter) {
    var mappings = {
        "0": "Today",
        "+1": "Tomorrow",
        "-1": "Yesterday",
    };
    return function(entry) {
        var d1 = moment.utc(entry);
        var d2 = moment.utc();
        var mappedValue = mappings[d1.diff(d2, 'days')];
        if (typeof mappedValue != 'undefined') {
            return mappedValue;
        } else {
            return d1.format("MM/DD/YYYY").toString();
        }
    }
});
app.filter('filterft', function() {
    return function(item) {
        output = item + "'";
        return output;
    };
});

app.filter('filterin', function() {
    return function(item) {
        output = item + '"';
        return output;
    };
});

app.filter('filterClock', function() {
    return function(item) {
        var output = 0;
        if (item <= 12) {
            if (item < 10) {
                output = '0' + item;
            } else {
                output = item;
            }
            output = parseFloat(parseFloat(output).toFixed(2)) + ' am';
        }
        if (item > 12 && item <= 24) {
            if ((item - 12) < 10) {
                output = '0' + (item - 12);
            } else {
                output = (item - 12);
            }

            output = parseFloat(parseFloat(output).toFixed(2)) + ' pm';
        }
        if (item > 24) {
            output = (item - 12);
            output = parseFloat(parseFloat(output).toFixed(2)) + ' am';
        }
        return output;
    };
});

app.filter('convertTime', function() {
    return function(item) {
        var output = 0;
        var time = item.split(' ');

        var hour = parseInt(time[0].split(':')[0]);
        var minute = parseInt(time[0].split(':')[1]);;
        if (time[1] == 'am') {
            if (hour >= 12) {
                output = (hour + 12);
            } else {
                output = hour;
            }
        }
        if (time[1] == 'pm') {
            if (hour >= 12) {
                output = 12 + (hour - 12);
            } else {
                output = hour + 12;
            }
        }

        return output + "." + minute;
    };
});

app.filter('filterClockSlider', function() {
    return function(item) {
        var output = 0;

        if (item > 0 && item < 12) {
            output = ((item < 10) ? '0' + item : item) + ':00 am';
        }
        if (item == 12) {
            output = '12:00 pm';
        }
        if (item > 12 && item < 24) {
            output = (((item - 12) < 10) ? '0' + (item - 12) : (item - 12)) + ':00 pm';
        }
        if (item == 0) {
            output = '12:00 am';
        }

        return output;
    };
});

app.filter('filterTime', function() {
    return function(item) {
        var output = '';
        if (item) {
            var arrTmp = (item).toString().split('.');
            if (Array.isArray(arrTmp) && arrTmp.length >= 2) {
                arrTmp.forEach(function(elem, index, array) {
                    if (elem < 10) {
                        output += '0' + elem + ':';
                    } else {
                        output += elem + ':';
                    }
                });
                output += '00';
            } else {
                if (item < 10) {
                    output = '0' + item;
                } else {
                    output = item;
                }
                output += ':00:00';
            }

        }

        return output;
    };
});

app.filter('filterClockEN', function() {
    return function(item) {
        var output = '';
        if (item) {
            var arrTmp = (item).toString().split('.');
            if (Array.isArray(arrTmp) && arrTmp.length >= 2) {
                if (arrTmp[0]) {
                    if (arrTmp[0] <= 12) {
                        if (arrTmp[0] < 10) {
                            output = '0' + arrTmp[0] + ':';;
                        } else {
                            output = arrTmp[0] + ':';
                        }
                        if (arrTmp[1] < 0) {
                            output += '0' + arrTmp[1] + ' am';
                        } else {
                            output += arrTmp[1] + ' am';
                        }
                    }
                    if (arrTmp[0] > 12 && arrTmp[0] <= 24) {
                        if ((arrTmp[0] - 12) < 10) {
                            output = '0' + (arrTmp[0] - 12) + ':';
                        } else {
                            output = (arrTmp[0] - 12) + ':';
                        }
                        if (arrTmp[1] < 0) {
                            output += '0' + arrTmp[1] + ' pm';
                        } else {
                            output += arrTmp[1] + ' pm';
                        }
                    }
                }
            } else {
                if (item <= 12) {
                    if (item < 10) {
                        output = '0' + item + ':';
                    } else {
                        output = item + ':'
                    }
                    output += '00 am';
                }
                if (item > 12 && item <= 24) {
                    if ((item - 12) < 10) {
                        output = '0' + (item - 12) + ':'
                    } else {
                        output = (item - 12) + ':'
                    }
                    output += '00 pm';
                }
            }
        }
        return output;
    }
});

app.filter('filterClockEnNumber', function() {
    return function(item) {
        var output = '';
        if (item) {
            var arrTmp = (item).toString().split('.');
            if (Array.isArray(arrTmp) && arrTmp.length >= 2) {
                if (arrTmp[0]) {
                    if (arrTmp[0] <= 12) {
                        if (arrTmp[0] < 10) {
                            output = '0' + arrTmp[0] + ':';;
                        } else {
                            output = arrTmp[0] + ':';
                        }
                        if (arrTmp[1] < 0) {
                            output += '0' + arrTmp[1];
                        } else {
                            output += arrTmp[1];
                        }
                    }
                    if (arrTmp[0] > 12 && arrTmp[0] <= 24) {
                        if ((arrTmp[0] - 12) < 10) {
                            output = '0' + (arrTmp[0] - 12) + ':';
                        } else {
                            output = (arrTmp[0] - 12) + ':';
                        }
                        if (arrTmp[1] < 0) {
                            output += '0' + arrTmp[1];
                        } else {
                            output += arrTmp[1];
                        }
                    }
                }
            } else {
                if (item <= 12) {
                    if (item < 10) {
                        output = '0' + item + ':';
                    } else {
                        output = item + ':'
                    }
                    output += '00';
                }
                if (item > 12 && item <= 24) {
                    if ((item - 12) < 10) {
                        output = '0' + (item - 12) + ':'
                    } else {
                        output = (item - 12) + ':'
                    }
                    output += '00';
                }
            }
        }
        return output;
    }
});


app.filter('sport_fitness', function() {
    return function(item) {
        output = item + "'";
        return output;
    };
});

app.filter('sport_icon', function() {
    return function(item) {
        output = item.replace('/', '-');
        return output;
    };
});

app.filter('avatar_type', function(ConfigApi) {
    return function(item) {
        console.log(item);
        if (item) {
            var valid = /^(ftp|http|https):\/\/[^ "]+$/.test(item);
            if (valid) {
                if (item.search(/https/) != -1) {
                    output = item
                } else {
                    output = item.replace('http', 'https');
                }
            } else {
                output = ConfigApi.serverStatic + item.replace('\\', '/');
            }

            return output;
        }else{
            return '../img/no-img.jpg';
        }
    };
});
app.filter('phonenumber', function() {

    return function(number) {
        if (!number) {
            return '';
        }

        number = String(number);
        var formattedNumber = number;

        number = number;

        var area = number.substring(0, 3);
        var front = number.substring(3, 6);
        var end = number.substring(6, 10);

        if (front) {
            formattedNumber = ("(" + area + ") " + front);
        }
        if (end) {
            formattedNumber += ("-" + end);
        }
        return formattedNumber;
    };
});
app.filter('tel', function () {
    return function (tel) {
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 1:
            case 2:
            case 3:
                city = value;
                break;

            default:
                city = value.slice(0, 3);
                number = value.slice(3);
        }

        if(number){
            if(number.length>3){
                number = number.slice(0, 3) + '-' + number.slice(3,7);
            }
            else{
                number = number;
            }

            return ("(" + city + ") " + number).trim();
        }
        else{
            return "(" + city;
        }

    };
});
